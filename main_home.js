let activeVideoId, imgTimeout, sliderActive = 3.5,
  botActive = 1, topActive = 3, buildedDeps = [], curId, curId2;

function scrollNavChange(){
  let navEl = document.getElementById('nav');

  if(window.pageYOffset > 0){
    if(!navEl.classList.contains('scrolled')) navEl.classList.add('scrolled');
  } else if(navEl.classList.contains('scrolled')){
    navEl.classList.remove('scrolled');
  }
}

function addBurgerEvent(){
  let burgerEl = document.getElementById('burger');

  burgerEl.addEventListener('click', function (){
    let navEl = document.getElementById('nav');

    if(!navEl.classList.contains('burger-on')){
      navEl.classList.add('burger-on');
    } else {
      navEl.classList.remove('burger-on');
    }
  });
}

function initVideoFrame(){
  let player;

  let videoBlock = document.getElementById('watchVideo');

  videoBlock.addEventListener('click', function() {
    player = new YT.Player('ytPlayer', {
      height: '100%',
      width: '100%',
      videoId: 'yKhn7n3b5yg',
      events: {
        'onReady': onPlayerReady
      }
    });

    function onPlayerReady() {
      if(!player) return;
      player.playVideo();

      document.documentElement.classList.add('video-on');
      activeVideoId = 'ytPlayer';
      document.addEventListener('keyup', trackEsc);
    }
  });

  document.getElementById('closeVideo').addEventListener('click', trackEsc);
}

function initVideoFrame2(){
  let player;

  let videoBlock = document.getElementById('watchVideo2');

  videoBlock.addEventListener('click', function() {
    player = new YT.Player('ytPlayer2', {
      height: '100%',
      width: '100%',
      videoId: curId,
      events: {
        'onReady': onPlayerReady
      }
    });

    function onPlayerReady() {
      if(!player) return;
      player.playVideo();

      document.documentElement.classList.add('video-on');
      activeVideoId = 'ytPlayer2';
      document.addEventListener('keyup', trackEsc);
    }
  });

  document.getElementById('closeVideo').addEventListener('click', trackEsc);
}

function initVideoFrame3(){
  let player;

  let videoBlock = document.getElementById('watchVideo3');

  videoBlock.addEventListener('click', function() {
    player = new YT.Player('ytPlayer3', {
      height: '100%',
      width: '100%',
      videoId: curId2,
      events: {
        'onReady': onPlayerReady
      }
    });

    function onPlayerReady() {
      if(!player) return;
      player.playVideo();

      document.documentElement.classList.add('video-on');
      activeVideoId = 'ytPlayer3';
      document.addEventListener('keyup', trackEsc);
    }
  });

  document.getElementById('closeVideo').addEventListener('click', trackEsc);
}

function trackEsc(e){
  if(e.keyCode == 27 || e.currentTarget.id == 'closeVideo'){
    let myFrame = document.getElementById(activeVideoId),
        mySpan = document.createElement('span'),
        parent = myFrame.parentNode;

    document.documentElement.classList.remove('video-on');
    parent.replaceChild(mySpan, myFrame);
    parent.getElementsByTagName('span')[0].id = activeVideoId;
  }
}

function changeCurVideo(id, active) {
  let curArr = testmVideos;

  if(id === 'curSlide2') {
    curArr = getScienceVideos();
    curId2 = curArr[active - 1].ytId;
  } else {
    curId = curArr[active - 1].ytId;
  }

  if(typeof loadVideoById === 'function') loadVideoById({ 'videoId': curId });
  document.getElementById(id).innerText = active;
}

function changeImg(id, active, videos) {
  let el = document.getElementById(id);
  el.classList.add('fade-out');

  if(imgTimeout) clearTimeout(imgTimeout);

  imgTimeout = setTimeout(function () {
    el.getElementsByTagName('img')[0].src = videos[active - 1].thumb;
    el.classList.remove('fade-out');
  }, 450);
}

// add thumbs and IDs for science section videos
function getScienceVideos(){
  return [{
    thumb: 'homepage_images/preview2.png',
    ytId: 'oqKHpoCg6rQ'
  }, {
    thumb: 'homepage_images/preview.png',
    ytId: 'AmaFqlxxmF8'
  }];
}

// slider func start -------------->
let testmVideos = [{
  thumb: 'homepage_images/thumb4.png',
  ytId: 'v6KsjYiEEe8'
}, {
  thumb: 'homepage_images/thumb3.png',
  ytId: 'kmfH7ujxqe0'
}, {
  thumb: 'homepage_images/preview.png',
  ytId: 'zUVUJ54BJTc'
}, {
  thumb: 'homepage_images/thumb2.png',
  ytId: 'oqKHpoCg6rQ'
}, {
  thumb: 'homepage_images/thumb5.png',
  ytId: '021RJd4kvTI'
}];

function getSliderLeftPos(){
  let curEl = document.getElementById('testmVideo').querySelector('img').src,
    bar = document.getElementById('testmBar'),
    activeBarWidth = 945/testmVideos.length;

  bar.style.width = activeBarWidth + 'px';

  for(let i = 0; i < buildedDeps.length; i++) {
    // ToDo to make it work localy change condition to ---> curEl.split('airo-naman-kumar/').pop() <----
    if(curEl.split('airo-naman-kumar').pop() === buildedDeps[i]){
      switch (i){
        case 0: sliderActive = 1;
          break;
        case 1: sliderActive = 2;
          break;
        case 2: sliderActive = 3;
          break;
        case 3: sliderActive = 4;
          break;
        case 4: sliderActive = 5;
          break;
      }
    }
  }

  bar.style.left = activeBarWidth*(sliderActive-1) + 'px';
}

function renderSlider(){
  let videoArr = testmVideos,
      videosEl = document.getElementById('testmVideos');


  while(videosEl.lastChild.id !== 'testmVideo') {
    videosEl.removeChild(videosEl.lastChild);
  }

  videosEl.lastChild.querySelector('img').src = videoArr[2].thumb;
  curId = videoArr[2].ytId;


  if(videoArr.length === 5) {
    attachImgEl('left-last', 0);
    attachImgEl('left-first', 1);
    attachImgEl('right-first', 3);
    attachImgEl('right-last', 4);
  } else if(videoArr.length === 3) {
    attachImgEl('left-first', 0);
    attachImgEl('right-first', 2);
  } else if(videoArr.length === 1) {
    return;
  }

  if(typeof loadVideoById === 'function') loadVideoById({ 'videoId': curId });

  function attachImgEl(className, index) {
    let videosEl = document.getElementById('testmVideos'),
      newEl = document.createElement('div'),
      newImgEl = document.createElement('img');

    newEl.classList.add(className);
    newImgEl.src = videoArr[index].thumb;
    newEl.appendChild(newImgEl);
    videosEl.appendChild(newEl);
    newEl.addEventListener('click', changeSlide);
  }
}

function changeSlide(e) {
  let val = e.currentTarget.classList.value,
    newTestm = [];

  if(val === 'right-first'){
    newTestm[0] = testmVideos[1];
    newTestm[1] = testmVideos[2];
    newTestm[2] = testmVideos[3];
    newTestm[3] = testmVideos[4];
    newTestm[4] = testmVideos[0];
  } else if(val === 'right-last'){
    newTestm[0] = testmVideos[2];
    newTestm[1] = testmVideos[3];
    newTestm[2] = testmVideos[4];
    newTestm[3] = testmVideos[0];
    newTestm[4] = testmVideos[1];
  } else if(val === 'left-first'){
    newTestm[0] = testmVideos[4];
    newTestm[1] = testmVideos[0];
    newTestm[2] = testmVideos[1];
    newTestm[3] = testmVideos[2];
    newTestm[4] = testmVideos[3];
  } else if(val === 'left-last'){
    newTestm[0] = testmVideos[3];
    newTestm[1] = testmVideos[4];
    newTestm[2] = testmVideos[0];
    newTestm[3] = testmVideos[1];
    newTestm[4] = testmVideos[2];
  }

  testmVideos = newTestm;
  renderSlider();
  getSliderLeftPos();
}

function buildBarDeps(){
  buildedDeps = [
    testmVideos[0].thumb,
    testmVideos[1].thumb,
    testmVideos[2].thumb,
    testmVideos[3].thumb,
    testmVideos[4].thumb,
  ];
}

// <-------------- slider func end

window.onload = function() {
  if(document.getElementById('invCount2')){
    document.getElementById('invCount2').innerText = 25 - document.getElementById('invitesused').innerText;
  }
  if(document.getElementById('invCount')){
    document.getElementById('invCount').innerText = 25 - document.getElementById('invitesused').innerText;
  }

  scrollNavChange();

  window.addEventListener('scroll', function(){
    scrollNavChange();
  });

  addBurgerEvent();

  initVideoFrame();
  initVideoFrame2();
  initVideoFrame3();

  (function setRefId() {
    var location = window.location.href;
    if(location.includes("ref_id")){
      id = location.split('ref_id=')[1].split('&')[0];
      if(id){
        document.getElementById('refId').value = id;
        document.getElementById('foot_refId').value = id;
      }
    }
  }());

  (function validateForm() {
    let form = document.getElementById('mainForm'),
      form2 = document.getElementById('footerForm'),
      mailInp = form.querySelector('input[name="mauticform[referralfield_email]"]'),
      mailInp2 = form2.querySelector('input[name="mauticform[referralfield_email]"]'),
      nameInp = form.querySelector('input[name="mauticform[referralfield_name]"]'),
      nameInp2 = form2.querySelector('input[name="mauticform[referralfield_name]"]');

    mailInp.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Email';});
    nameInp.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Name';});
    mailInp2.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Email';});
    nameInp2.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Name';});

    form.addEventListener('submit', function(e) {
      formSubmitFunction(e);
    });

    form2.addEventListener('submit', function(e) {
      formSubmitFunction(e);
    });

    function formSubmitFunction(e) {
      let name, email;

      if(e.currentTarget.id === 'mainForm'){
        name = nameInp;
        email = mailInp;
      } else {
        name = nameInp2;
        email = mailInp2;
      }

      if (!name.value) {
        e.preventDefault();
        name.classList = 'no-value required';
        name.placeholder = 'This field is required!';
      } else {
        name.classList = '';
      }

      if (email.value) {
        if (!(/^[a-z\.\_0-9]+@[a-z]+\.[a-z]+$/i.test(email.value))) {
          email.value = '';
        }
      }

      if (!email.value) {
        e.preventDefault();
        email.classList = 'no-value required';
        email.placeholder = 'This field is required!';
      } else {
        email.classList = '';
      }
    }
  })();

  (function customSelect() {

    let selectEl = document.getElementById('customSelect')
    if(!selectEl) return;

    let optionEl = selectEl.getElementsByTagName('span');

    selectEl.addEventListener('click', function(){
      if(selectEl.className.indexOf( 'collapsed') < 0) selectEl.className = 'no-value collapsed';
      else selectEl.className = '';
    });

    for(let i=0; i < optionEl.length; i++){

      optionEl[i].addEventListener('click', function(){
        this.parentNode.dataset.value = this.dataset.value;
        document.getElementById('frequency').value = this.dataset.value;
      });
    }
  })();

  (function addSliderCarousel() {
    buildBarDeps();
    getSliderLeftPos();
    renderSlider();
  })();

  (function addTopVideoCarousel() {
    let videoArray = testmVideos;

    if(videoArray.length === 1) document.getElementById('rightArrow').classList.add('disabled');

    document.getElementById('maxSlide').innerText = videoArray.length;
    document.getElementById('curSlide').innerText = topActive;
    document.getElementById('leftArrow').addEventListener('click', changeVideo);
    document.getElementById('rightArrow').addEventListener('click', changeVideo);

    function changeVideo(e){
      if(e.currentTarget.id === 'leftArrow'){
        if(topActive === 1) return;
        if(topActive === videoArray.length) document.getElementById('rightArrow').classList.remove('disabled');

        topActive = topActive - 1;
        changeCurVideo('curSlide', topActive);
        changeImg('testmVideo', topActive, videoArray);
        if(topActive === 1) e.currentTarget.classList.add('disabled');
      } else if(e.currentTarget.id === 'rightArrow'){
        if(topActive === videoArray.length) return;
        if(topActive === 1) document.getElementById('leftArrow').classList.remove('disabled');

        topActive = topActive + 1;
        changeCurVideo('curSlide', topActive);
        changeImg('testmVideo', topActive, videoArray);
        if(topActive === videoArray.length) e.currentTarget.classList.add('disabled');
      }
    }

  })();

  (function addBotVideoCarousel() {
    let videoArray = getScienceVideos();

    curId2 = videoArray[0].ytId;

    if(videoArray.length === 1) document.getElementById('rightArrow2').classList.add('disabled');

    document.getElementById('maxSlide2').innerText = videoArray.length;
    document.getElementById('leftArrow2').addEventListener('click', changeVideo);
    document.getElementById('rightArrow2').addEventListener('click', changeVideo);

    function changeVideo(e){
      if(e.currentTarget.id === 'leftArrow2'){
        if(botActive === 1) return;
        if(botActive === videoArray.length) document.getElementById('rightArrow2').classList.remove('disabled');

        botActive = botActive - 1;
        changeCurVideo('curSlide2', botActive);
        changeImg('watchVideo3', botActive, videoArray);
        if(botActive === 1) e.currentTarget.classList.add('disabled');
      } else if(e.currentTarget.id === 'rightArrow2'){
        if(botActive === videoArray.length) return;
        if(botActive === 1) document.getElementById('leftArrow2').classList.remove('disabled');

        botActive = botActive + 1;
        changeCurVideo('curSlide2', botActive);
        changeImg('watchVideo3', botActive, videoArray);
        if(botActive === videoArray.length) e.currentTarget.classList.add('disabled');
      }
    }
  })();

  document.body.classList.remove('loading-dom');
};