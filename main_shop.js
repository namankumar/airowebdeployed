let activeImage = 0;

function scrollNavChange(){
  let navEl = document.getElementById('nav');

  if(window.pageYOffset > 0){
    if(!navEl.classList.contains('scrolled')) navEl.classList.add('scrolled');
  } else if(navEl.classList.contains('scrolled')){
    navEl.classList.remove('scrolled');
  }
}

function addBurgerEvent(){
  let burgerEl = document.getElementById('burger');

  burgerEl.addEventListener('click', function (){
    let navEl = document.getElementById('nav');

    if(!navEl.classList.contains('burger-on')){
      navEl.classList.add('burger-on');
    } else {
      navEl.classList.remove('burger-on');
    }
  });
}

function addQuantityEvent(){
  let addQuantEl = document.getElementById('plus');

  addQuantEl.addEventListener('click', function (){
    let quantText = document.getElementById('quant'),
      currentQuant = parseInt(quantText.innerHTML, 10),
      priceText = document.getElementById('price');

    document.getElementById('minus').classList.remove('disabled');

    priceText.innerHTML = '$' + parseInt(initPrice * (currentQuant + 1), 10) + ' USD';
    quantText.innerHTML = currentQuant + 1;
  });
}

function addRemoveQuantityEvent(){
  let removeQuantEl = document.getElementById('minus');

  removeQuantEl.addEventListener('click', function (){
    let quantText = document.getElementById('quant'),
      currentQuant = parseInt(quantText.innerHTML, 10),
      priceText = document.getElementById('price');

    if(currentQuant == 1) return;
    else if(currentQuant == 2) removeQuantEl.classList.add('disabled');

    priceText.innerHTML = '$' + parseInt(initPrice * (currentQuant - 1), 10) + ' USD';
    quantText.innerHTML = currentQuant - 1;
  });
}

function addColorChangeEvent() {
  let elements = document.querySelectorAll('.select-option-color');

  for (let i = 0; i < elements.length; i++) {

    elements[i].addEventListener("click", function() {
      activeEl.classList.remove('selected');
      changeColor(elements[i]);
    });
  }
}

function changeColor(colorItem){
  initPrice = colorItem.dataset.price;
  activeEl = colorItem;

  colorItem.classList.add('selected');
  document.getElementById('quant').innerHTML = '1';
  document.getElementById('price').innerHTML = '$' + initPrice + ' USD';
}

function generateBuyUrl(){

  let color = document.querySelector('.select-option-color.selected').dataset.color,
      quantity = document.getElementById('quant').innerHTML;

  window.open('https://airo-health.myshopify.com/cart/49688815893:' + quantity, '_blank');
}

let initPrice = 129.99,
    activeEl = document.querySelector('.selected');

window.onload = function() {

  scrollNavChange();

  document.addEventListener('scroll', function(){
    scrollNavChange();
  });

  addBurgerEvent();

  document.getElementById('buyAiro').addEventListener('click', generateBuyUrl);

  addQuantityEvent();

  addRemoveQuantityEvent();

 //disabled till user interface is made less confusing
  addColorChangeEvent();

  (function setRefId() {
    var location = window.location.href;
    if(location.includes("ref_id")){
      id = location.split('ref_id=')[1].split('&')[0];
      if(id){
          document.getElementById('refId').value = id;
          document.getElementById('foot_refId').value = id;
      }
    }
  }());


  (function validateForm() {
    let form = document.getElementById('footerForm'),
      mailInp = form.querySelector('input[name="mauticform[referralfield_name]"]'),
      nameInp = form.querySelector('input[name="mauticform[referralfield_email]"]');

    mailInp.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Email';});
    nameInp.addEventListener('keypress', function(){ this.classList = ''; this.placeholder = 'Name';});

    form.addEventListener('submit', function(e) {
      formSubmitFunction(e);
    });

    function formSubmitFunction(e) {
      if (!nameInp.value) {
        e.preventDefault();
        nameInp.classList = 'no-value required';
        nameInp.placeholder = 'This field is required!';
      } else {
        nameInp.classList = '';
      }

      if (mailInp.value) {
        if (!(/^[a-z\.\_0-9]+@[a-z]+\.[a-z]+$/i.test(mailInp.value))) {
          mailInp.value = '';
        }
      }

      if (!mailInp.value) {
        e.preventDefault();
        mailInp.classList = 'no-value required';
        mailInp.placeholder = 'This field is required!';
      } else {
        mailInp.classList = '';
      }
    }
  })();

  (function addImagesCarousel(){
    let leftMove = document.getElementById('leftArrow'),
        rightMove = document.getElementById('rightArrow'),
        imageElms = document.getElementById('images').getElementsByClassName('order__img');

    rightMove.addEventListener('click', changeImages);
    leftMove.addEventListener('click', changeImages);

    function changeImages(e){
      if(e.currentTarget.id === 'leftArrow'){
        if(activeImage <= 0) return;
        if(activeImage === imageElms.length - 1) rightMove.classList.remove('disabled');

        activeImage = activeImage - 1;
        if(activeImage === 0) leftMove.classList.add('disabled');
        changePosition(activeImage);
      } else {
        if(activeImage >= imageElms.length - 1) return;
        if(activeImage === 0) leftMove.classList.remove('disabled');

        activeImage = activeImage + 1;
        if(activeImage === imageElms.length - 1) rightMove.classList.add('disabled');
        changePosition(activeImage);
      }

      function changePosition(active){
        for(let i = 0; i < imageElms.length; i++){
          imageElms[i].style.left = active*(-100) + '%';
        }
      }
    }
  })();

  document.body.classList.remove('loading-dom');
};

